# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

Types of changes :
- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## [Unreleased]

## [1.1] - 2020-02-02
### Fixed
- Drupal coding standard

## [1.0] - 2020-02-02
### Added
- init

[Unreleased]: https://git.drupal.org/project/aaabc_d/compare/7.x-1.0...HEAD
[1.1]: https://git.drupal.org/project/aaabc_d/compare/7.x-1.0...7.x-1.1
[1.0]: https://git.drupal.org/project/aaabc_d/tree/7.x-1.0
